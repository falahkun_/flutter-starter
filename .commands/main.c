#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>

int main() {
    char feature_name[50];

    // Meminta input dari pengguna
    printf("Masukkan nama fitur: ");
    scanf("%s", feature_name);

    if (chdir("..") != 0) {
        perror("Gagal mengganti direktori");
        return 1;
    }

    if (chdir("features") != 0) {
        perror("Gagal masuk ke direktori lain");
        return 1;
    }

    // Membangun dan menjalankan perintah bash dengan nama fitur
    char command[100];
    sprintf(command, "flutter create --template=package %s", feature_name);
    system(command);

    char directory[100];
    if (chdir(feature_name) != 0) {
        perror("Gagal masuk ke direktori lain");
        return 1;
    }

    system("rm -rf .dart_tool");
    system("rm -rf .idea");
    system("rm .gitignore");
    system("rm analysis_options.yaml");
    system("rm CHANGELOG.md");
    system("rm LICENSE");
    system("rm README.md");
    system("rm splash.iml");
    system("rm pubspec.lock");
    system("rm pubspec.yaml");

    FILE *testFile = fopen("pubspec.yaml", "w");
    if (testFile == NULL) {
        perror("Gagal membuka test.yaml");
        return 1;
    }

    fprintf(testFile, "name: %s\n", feature_name);
    fprintf(testFile, "description: A new flutter package\n");
    fprintf(testFile, "publish_to: none\n");
    fprintf(testFile, "\n");
    fprintf(testFile, "version: 1.0.0+1\n");
    fprintf(testFile, "\n");
    fprintf(testFile, "environment: \n");
    fprintf(testFile, " sdk: '>=2.19.6 <3.0.0'\n");
    fprintf(testFile, " flutter: '>=1.17.0'\n");
    fprintf(testFile, "\n");
    fprintf(testFile, "dependencies: \n");
    fprintf(testFile, " core: \n");
    fprintf(testFile, "  path: ../../libraries/core\n");
    fprintf(testFile, " flutter: \n");
    fprintf(testFile, "  sdk: flutter\n");
    fprintf(testFile, " shared: \n");
    fprintf(testFile, "  path: ../../libraries/shared\n");

    fprintf(testFile, "\n");
    fprintf(testFile, "dev_dependencies: \n");
    fprintf(testFile, " build_runner: ^2.3.3\n");
    fprintf(testFile, " flutter_lints: ^2.0.0\n");
    fprintf(testFile, " flutter_test: \n");
    fprintf(testFile, "  sdk: flutter\n");
    fprintf(testFile, " json_serializable: ^6.6.1\n");
    fprintf(testFile, " mocktail: ^0.3.0\n");
    fclose(testFile);

    system("flutter pub get");


    return 0;
}