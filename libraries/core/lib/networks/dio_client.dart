// ignore_for_file: depend_on_referenced_packages

import 'dart:io';

import 'package:core/core.dart';
import 'package:dio/dio.dart';
import 'package:dio_smart_retry/dio_smart_retry.dart';
import 'package:shared/shared.dart';

class DioClient {
  DioClient(
    this.dio, {
    this.token,
  }) {
    client = dio;
    client
      ..options = BaseOptions(
        baseUrl: AppConfig.baseUrl.value.toString(),
      )
      ..interceptors.addAll(<Interceptor>[
        LogInterceptor(
          requestBody: true,
          responseBody: true,
          // logPrint: (data) {
          //   AppLogger().print(data.toString());
          // },
        ),
        if (allowRetryWhenError)
          RetryInterceptor(
            dio: dio,
            retries: dioRetryCount,
            logPrint: (message) =>
                AppLogger(appName: 'DioRetryInterceptor').print(message),
          ),
        InterceptorsWrapper(
          onRequest: (options, handler) async {
            if (token != null) {
              options.headers.addAll(<String, dynamic>{
                HttpHeaders.authorizationHeader: 'Bearer $token',
              });
            }
            handler.next(options);
          },
        )
      ]);
  }

  final Dio dio;
  final String? token;

  late Dio client;
}
