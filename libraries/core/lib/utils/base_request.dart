// ignore_for_file: use_if_null_to_convert_nulls_to_bools

import 'dart:developer';

import 'package:core/core.dart';
import 'package:dio/dio.dart';
import 'package:shared/shared.dart';

enum RequestMethod { get, post, patch, delete }

class BaseRequest {
  const BaseRequest(this.dio);

  final Dio dio;

  Future<Map<String, dynamic>> baseRequest(
    RequestMethod method,
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? data,
    int successStatusCode = 200,
  }) async {
    late Response<Map<String, dynamic>> response;
    log('$method(${dio.options.baseUrl} $path, params: $queryParameters, data: $data)');
    try {
      switch (method) {
        case RequestMethod.get:
          response = await dio.get<Map<String, dynamic>>(
            path,
            queryParameters: queryParameters,
          );
          break;
        case RequestMethod.post:
        if (data != null) {
          dio.options.headers =  <String, dynamic>{
            'Content-Type': 'application/json',
          };
        } else {
          dio.options.headers =  <String, dynamic>{
            'Content-Type': 'application/x-www-form-urlencoded',
          };
        }
          response = await dio.post<Map<String, dynamic>>(
            path,
            data: data,
            // queryParameters: queryParameters,
          );
          break;
        case RequestMethod.patch:
          dio.options.headers = <String, dynamic>{
            'Content-Type': 'application/json',
          };
          response = await dio.put<Map<String, dynamic>>(
            path,
            data: data,
            // queryParameters: queryParameters,
          );
          break;
        case RequestMethod.delete:
          break;
      }

      if (response.statusCode == successStatusCode) {

        if (response.data!.containsKey('error') == true) {
          throw GeneralServerException(message: response.data!['error']);
        }
        return response.data!;
      }
      return response.data!;
    } on DioError catch (err) {
      throw GeneralServerException(message: err.toServerException().message);
    } catch (err) {
      throw const ErrorCodeException(message: 'CODE_ERROR');
    }
  }

  Future<Map<String, dynamic>?> get(
    String path, {
    Map<String, dynamic>? queryParameters,
    // Map<String, dynamic>? data,
    int successStatusCode = 200,
  }) =>
      baseRequest(
        RequestMethod.get,
        path,
        queryParameters: queryParameters,
        successStatusCode: successStatusCode,
      );

  Future<Map<String, dynamic>?> post(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? data,
    int successStatusCode = 200,
  }) =>
      baseRequest(
        RequestMethod.post,
        path,
        queryParameters: queryParameters,
        data: data,
        successStatusCode: successStatusCode,
      );

  Future<Map<String, dynamic>?> put(
    String path, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? data,
    int successStatusCode = 200,
  }) =>
      baseRequest(
        RequestMethod.patch,
        path,
        queryParameters: queryParameters,
        data: data,
        successStatusCode: successStatusCode,
      );
}
