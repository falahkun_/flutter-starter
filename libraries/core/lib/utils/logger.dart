import 'dart:developer';
import 'package:logger/logger.dart';

class AppLogger {
  AppLogger({this.appName = 'LOG'});

  final String appName;

  final logger = Logger();

  void print([String message = '', StackTrace? stackTrace]) {
    logger.d(
      message,
      stackTrace: stackTrace,
      time: DateTime.now(),

    );

    log(message, 
      name: appName, 
      stackTrace: stackTrace, 
      time: DateTime.now(),
    );
  }
}
