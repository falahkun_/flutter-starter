// ignore_for_file: depend_on_referenced_packages

import 'package:core/core.dart';
import 'package:core/networks/dio_client.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared/shared.dart';

void main() {
  group('/networks', () {
    test('Test dio with custom url dev', () {
      F.flavor = Flavor.dev;
      final matcher = _setUpUrl(F.flavor);
      final dio = Dio();
      final dioClient = DioClient(dio);

      expect(dioClient.client.options.baseUrl, matcher);
    });
    test('Test dio with custom url stag', () {
      F.flavor = Flavor.stag;
      final matcher = _setUpUrl(F.flavor);
      final dio = Dio();
      final dioClient = DioClient(dio);

      expect(dioClient.client.options.baseUrl, matcher);
    });
    test('Test dio with custom url prod', () {
      F.flavor = Flavor.prod;
      final matcher = _setUpUrl(F.flavor);
      final dio = Dio();
      final dioClient = DioClient(dio);

      expect(dioClient.client.options.baseUrl, matcher);
    });
  });
}

String _setUpUrl(Flavor flavor) {
  late String scheme;
  late String host;
  late int? port;
  late String path;
  switch (flavor) {
    case Flavor.dev:
      scheme = baseSchemeDev;
      host = baseHostDev;
      port = basePortDev;
      path = basePathDev;
      break;
    case Flavor.prod:
      scheme = baseSchemeProd;
      host = baseHostProd;
      port = basePortProd;
      path = basePathProd;
      break;
    case Flavor.stag:
      scheme = baseSchemeStag;
      host = baseHostStag;
      port = basePortStag;
      path = basePathStag;
      break;
  }

  var url = '$scheme://$host';

  if (port != null) {
    url += ':$port';
  }

  return '$url/$path';
}
