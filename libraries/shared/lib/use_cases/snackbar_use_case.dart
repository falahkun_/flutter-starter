import 'dart:async';

import 'package:core/core.dart';
import 'package:dartz/dartz.dart';
import 'package:shared/helpers/helpers.dart';
import 'package:shared/shared.dart';

class SnackbarUseCase implements UseCaseFuture<Failure, bool, MessageEntity> {
  SnackbarUseCase({required this.helper});

  final SnackbarHelper helper;

  @override
  FutureOr<Either<Failure, bool>> call(MessageEntity params) async =>
      await helper.showSnackbar(params);
}
