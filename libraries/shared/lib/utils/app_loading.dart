import 'package:core/core.dart';
import 'package:flutter/material.dart';
import 'package:shared/shared.dart';

class AppLoading {
  const AppLoading._();

  static BuildContext? _buildContext;

  static final _logger = AppLogger(appName: 'AppLoading');

  static void show(BuildContext context) {
    if (_buildContext == null) {
      _buildContext = context;
      _logger.print('Loading is Appear!');
      showDialog<void>(
        context: context,
        barrierColor: const Color(0xFF344054).withOpacity(.8),
        barrierDismissible: false,
        builder: (_) {
          return const Material(
            color: Colors.transparent,
            child: LoadingWidget(),
          );
        },
      );
    }
  }

  static bool get isLoading {
    return _buildContext != null;
  }

  static void hide() {
    if (_buildContext != null) {
      if (Navigator.canPop(_buildContext!)) {
        _logger.print('Loading is Hidden!');
        Navigator.pop(_buildContext!);
        _buildContext = null;
      }
    }
  }
}
