import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class MessageEntity extends Equatable {
  const MessageEntity({
    required this.title,
    required this.message,
    this.context,
  });

  final String title;
  final String? message;
  final BuildContext? context;

  @override
  List<Object?> get props => [
        title,
        message,
      ];
}
