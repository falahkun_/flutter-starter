// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'roles_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RolesModel _$RolesModelFromJson(Map<String, dynamic> json) => RolesModel(
      name: json['name'] as String,
      label: json['label'] as String,
    );

Map<String, dynamic> _$RolesModelToJson(RolesModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'label': instance.label,
    };
