import 'package:equatable/equatable.dart';
import 'package:shared/shared.dart';

class CountryCode extends Equatable {
  const CountryCode({
    required this.flag,
    required this.countryName,
    required this.countryCode,
    required this.phoneCode,
  });

  static final countries = _listCountryCode;

  final String flag;
  final String countryName;
  final String countryCode;
  final String phoneCode;

  /// [combine] use to get the phone number with prefix country code
  String combine(String phoneNumber, {bool usePlusPrefix = false,}) {
    var result = phoneCode + phoneNumber;
    if (usePlusPrefix) {
      result = '+$result';
    }
    return result;
  }

  @override
  List<Object?> get props => [
        flag,
        countryName,
        countryCode,
        phoneCode,
      ];
}

List<CountryCode> _listCountryCode = [
  const CountryCode(
    flag: MainAssets.fId,
    countryName: 'Indonesia',
    countryCode: 'id',
    phoneCode: '62',
  ),
  const CountryCode(
    flag: MainAssets.fEn,
    countryName: 'Indonesia',
    countryCode: 'id',
    phoneCode: '62',
  ),
];
