import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'roles_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class RolesModel extends Equatable {
  const RolesModel({
    required this.name,
    required this.label,
  });

  factory RolesModel.fromJson(Map<String, dynamic> json) =>
      _$RolesModelFromJson(json);

  final String name;
  final String label;

  Map<String, dynamic> toJson() => _$RolesModelToJson(this);

  @override
  List<Object?> get props => [
        name,
        label,
      ];
}
