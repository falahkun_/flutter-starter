import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const MaterialColor primary = MaterialColor(
    0xFF0D9ABD,
    <int, Color>{
      100: Color(0xFFCFEBF2),
      200: Color(0xFFAEDDE9),
      300: Color(0xFF86CCDE),
      400: Color(0xFF5EBCD3),
      500: Color(0xFF35ABC8),
      600: Color(0xFF0D9ABD),
      700: Color(0xFF0B809D),
      800: Color(0xFF09677E),
      900: Color(0xFF064D5E),
    },
  );

  static const MaterialColor gray = MaterialColor(0xFF667085, <int, Color>{
    25: Color(0xFFFCFCFD),
    50: Color(0xFFF9FAFB),
    100: Color(0xFFF2F4F7),
    200: Color(0xFFEAECF0),
    300: Color(0xFFD0D5DD),
    400: Color(0xFF98A2B3),
    500: Color(0xFF667085),
    600: Color(0xFF475467),
    700: Color(0xFF344054),
    800: Color(0xFF1D2939),
    900: Color(0xFF101828),
  });

  final color = AppColors.primary[25];
}
