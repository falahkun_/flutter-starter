import 'package:flutter/material.dart';

class CustomTextStyles {
  CustomTextStyles._();

  static const TextStyle _baseStyle = TextStyle(
    fontFamily: 'Inter',
  );

  //Regular weight
  static TextStyle textRegularXS =
      _baseStyle.copyWith(fontSize: 12, fontWeight: FontWeight.w400);
  static TextStyle textRegularS =
      _baseStyle.copyWith(fontSize: 14, fontWeight: FontWeight.w400);
  static TextStyle textRegularM =
      _baseStyle.copyWith(fontSize: 16, fontWeight: FontWeight.w400);
  static TextStyle textRegularL =
      _baseStyle.copyWith(fontSize: 18, fontWeight: FontWeight.w400);
  static TextStyle textRegularXL =
      _baseStyle.copyWith(fontSize: 20, fontWeight: FontWeight.w400);
  static TextStyle textRegularHeaderXS =
      _baseStyle.copyWith(fontSize: 24, fontWeight: FontWeight.w400);
  static TextStyle textRegularHeaderS =
      _baseStyle.copyWith(fontSize: 30, fontWeight: FontWeight.w400);
  static TextStyle textRegularHeaderM =
      _baseStyle.copyWith(fontSize: 36, fontWeight: FontWeight.w400);
  static TextStyle textRegularHeaderL =
      _baseStyle.copyWith(fontSize: 48, fontWeight: FontWeight.w400);
  static TextStyle textRegularHeaderXL =
      _baseStyle.copyWith(fontSize: 60, fontWeight: FontWeight.w400);
  static TextStyle textRegularHeaderXXL =
      _baseStyle.copyWith(fontSize: 72, fontWeight: FontWeight.w400);

  //Medium weight
  static TextStyle textMediumXS =
      _baseStyle.copyWith(fontSize: 12, fontWeight: FontWeight.w500);
  static TextStyle textMediumS =
      _baseStyle.copyWith(fontSize: 14, fontWeight: FontWeight.w500);
  static TextStyle textMediumM =
      _baseStyle.copyWith(fontSize: 16, fontWeight: FontWeight.w500);
  static TextStyle textMediumL =
      _baseStyle.copyWith(fontSize: 18, fontWeight: FontWeight.w500);
  static TextStyle textMediumXL =
      _baseStyle.copyWith(fontSize: 20, fontWeight: FontWeight.w500);
  static TextStyle textMediumHeaderXS =
      _baseStyle.copyWith(fontSize: 24, fontWeight: FontWeight.w500);
  static TextStyle textMediumHeaderS =
      _baseStyle.copyWith(fontSize: 30, fontWeight: FontWeight.w500);
  static TextStyle textMediumHeaderM =
      _baseStyle.copyWith(fontSize: 36, fontWeight: FontWeight.w500);
  static TextStyle textMediumHeaderL =
      _baseStyle.copyWith(fontSize: 48, fontWeight: FontWeight.w500);
  static TextStyle textMediumHeaderXL =
      _baseStyle.copyWith(fontSize: 60, fontWeight: FontWeight.w500);
  static TextStyle textMediumHeaderXXL =
      _baseStyle.copyWith(fontSize: 72, fontWeight: FontWeight.w500);

  //Semibold weight
  static TextStyle textSemiboldXS =
      _baseStyle.copyWith(fontSize: 12, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldS =
      _baseStyle.copyWith(fontSize: 14, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldM =
      _baseStyle.copyWith(fontSize: 16, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldL =
      _baseStyle.copyWith(fontSize: 18, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldXL =
      _baseStyle.copyWith(fontSize: 20, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldHeaderXS =
      _baseStyle.copyWith(fontSize: 24, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldHeaderS =
      _baseStyle.copyWith(fontSize: 30, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldHeaderM =
      _baseStyle.copyWith(fontSize: 36, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldHeaderL =
      _baseStyle.copyWith(fontSize: 48, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldHeaderXL =
      _baseStyle.copyWith(fontSize: 60, fontWeight: FontWeight.w600);
  static TextStyle textSemiboldHeaderXXL =
      _baseStyle.copyWith(fontSize: 72, fontWeight: FontWeight.w600);

  //Bold weight
  static TextStyle textBoldXS =
      _baseStyle.copyWith(fontSize: 12, fontWeight: FontWeight.w700);
  static TextStyle textBoldS =
      _baseStyle.copyWith(fontSize: 14, fontWeight: FontWeight.w700);
  static TextStyle textBoldM =
      _baseStyle.copyWith(fontSize: 16, fontWeight: FontWeight.w700);
  static TextStyle textBoldL =
      _baseStyle.copyWith(fontSize: 18, fontWeight: FontWeight.w700);
  static TextStyle textBoldXL =
      _baseStyle.copyWith(fontSize: 20, fontWeight: FontWeight.w700);
  static TextStyle textBoldHeaderXS =
      _baseStyle.copyWith(fontSize: 24, fontWeight: FontWeight.w700);
  static TextStyle textBoldHeaderS =
      _baseStyle.copyWith(fontSize: 30, fontWeight: FontWeight.w700);
  static TextStyle textBoldHeaderM =
      _baseStyle.copyWith(fontSize: 36, fontWeight: FontWeight.w700);
  static TextStyle textBoldHeaderL =
      _baseStyle.copyWith(fontSize: 48, fontWeight: FontWeight.w700);
  static TextStyle textBoldHeaderXL =
      _baseStyle.copyWith(fontSize: 60, fontWeight: FontWeight.w700);
  static TextStyle textBoldHeaderXXL =
      _baseStyle.copyWith(fontSize: 72, fontWeight: FontWeight.w700);
}
