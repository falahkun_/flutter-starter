class MainAssets {
  const MainAssets._();

  static const String _path = 'assets/images';

  /* ------------------------------FLAGS------------------------------ */
  static const String fId = '$_path/flags/id.png';
  static const String fEn = '$_path/flags/en.png';

  /* ------------------------------ICONS------------------------------ */

  /* --------------------------ILLUSTRATIONS-------------------------- */
  static const String logo = '$_path/illustrations/logo.png';
  static const String hris = '$_path/illustrations/HRIS.png';
}
