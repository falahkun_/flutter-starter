import 'dart:async';

import 'package:geolocator/geolocator.dart';

class LocationHelper {
  LocationHelper() {
    _checkLocationStatus();
    _subscribeToLocationStatusChanges();
  }

  final StreamController<ServiceStatus> _locationStatusController =
      StreamController<ServiceStatus>.broadcast();

  Stream<ServiceStatus> get locationStatusStream =>
      _locationStatusController.stream;

  Future<void> _checkLocationStatus() async {
    final isLocationServiceEnabled =
        await Geolocator.isLocationServiceEnabled();
    _locationStatusController.add(
      isLocationServiceEnabled ? ServiceStatus.enabled : ServiceStatus.disabled,
    );
  }

  void _subscribeToLocationStatusChanges() {
    Geolocator.getPositionStream(
      locationSettings: const LocationSettings(
        distanceFilter: 10,
      ),
    ).listen(
      (_) {},
      onError: (dynamic error) {
        if (error is LocationServiceDisabledException) {
          _locationStatusController.add(
            ServiceStatus.disabled,
          );
        }
      },
    );
  }

  void dispose() {
    _locationStatusController.close();
  }
}
