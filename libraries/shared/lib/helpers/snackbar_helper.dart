import 'package:core/core.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:shared/shared.dart';

class SnackbarHelper {
  Future<Either<Failure, bool>> showSnackbar(
    MessageEntity message,
  ) async {

    if (message.context == null) {
      return const Left(CodeFailure(message: 'CONTEXT_REQUIRED'));
    }

    final snackBar = SnackBar(
      content: Text(message.title),
    ); 

    ScaffoldMessenger.of(message.context!).showSnackBar(snackBar);
    return const Right(true);
  }
}
