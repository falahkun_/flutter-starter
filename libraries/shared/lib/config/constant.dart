const String baseHostDev = 'app.kotapedia.id';
const String baseHostStag = 'app.kotapedia.id';
const String baseHostProd = 'app.kotapedia.id';

const String baseSchemeDev = 'https';
const String baseSchemeStag = 'https';
const String baseSchemeProd = 'https';

int? basePortDev;
int? basePortStag;
int? basePortProd;

const String basePathDev = 'api/';
const String basePathStag = 'api/';
const String basePathProd = 'api/';

/// count for dio retrying if transaction error
const int dioRetryCount = 5;
const bool allowRetryWhenError = true;
