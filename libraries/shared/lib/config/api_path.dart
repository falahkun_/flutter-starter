class ApiPath {
  static const String auth = 'auth/';
  static const String authLogin = 'v1/$auth/login';
  static const String authRegister = 'v1/$auth/register';
  static const String authForgotPassword = 'v1/$auth/password/email';

  static const String mobileMenu = 'v1/getMenuMobile';
  static const String news = 'v1/cms/news';

  static const String getSlider = 'v1/getSlider';
  static const String getRecommendation = 'v1/getRekomendasiDestinasi';
  static const String getDetailDirectory = 'v1/getDirectoryDetail';

}
