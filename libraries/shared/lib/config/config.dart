import 'package:core/core.dart';
import 'package:shared/shared.dart';

export 'api_path.dart';
export 'constant.dart';
export 'named_routes.dart';

class AppConfig {
  static const String appName = 'Kotapedia';
  static const baseTheme = AppTheme.light;

  static FlavorConfig<Uri> baseUrl = FlavorConfig<Uri>(
    dev: Uri(
      host: baseHostDev,
      path: basePathDev,
      port: basePortDev,
      scheme: baseSchemeDev,
    ),
    prod: Uri(
      host: baseHostProd,
      path: basePathProd,
      port: basePortProd,
      scheme: baseSchemeProd,
    ),
    staging: Uri(
      host: baseHostStag,
      path: basePathStag,
      port: basePortStag,
      scheme: baseSchemeStag,
    ),
  );
}
