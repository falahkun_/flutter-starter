class NamedRoutes {
  NamedRoutes._();

  static const String initial = '/';
  static const String settings = '/settings';
  static const String home = '/home';
  static const String login = '/login';
  static const String register = '/register';
  static const String forgotPassword = '/forgot-password';
  static const String onboarding = '/onboarding';
  static const String directory = '/directory';
}
