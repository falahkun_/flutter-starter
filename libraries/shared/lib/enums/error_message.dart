// ignore_for_file: constant_identifier_names

enum ErrorMessage {
  ISSUE_NOT_FOUND,
  socket,
  USER_INACTIVE,
  CREDENTIAL_INVALID,
  UNSUPPORTED_FILE,
  PERMISSION_DENIED,
  CODE_ERROR,
  PLATFORM_NOT_SUPPORTED,
  EMAIL_INVALID,
  CONTEXT_REQUIRED
}

ErrorMessage? errorMessagefromString(String value) {
  try {
    return ErrorMessage.values
        .where((element) => element.name.toUpperCase() == value.toUpperCase())
        .single;
  } catch (err) {
    return null;
  }
}
