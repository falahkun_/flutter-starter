// ignore_for_file: prefer_null_aware_method_calls

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RatingWidget extends StatelessWidget {
  const RatingWidget({
    required this.initialRating,
    this.onRatingUpdate,
    super.key,
    this.iconPadding = 0,
    this.iconSize = 24,
  });

  final double initialRating;
  final void Function(double)? onRatingUpdate;
  final double iconSize;
  final double iconPadding;

  @override
  Widget build(BuildContext context) {
    return RatingBar.builder(
      initialRating: initialRating,
      minRating: 1,
      allowHalfRating: true,
      ignoreGestures: onRatingUpdate == null,
      tapOnlyMode: true,
      itemPadding: EdgeInsets.only(right: iconPadding),
      itemSize: iconSize,
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.amber,
        size: iconSize,
      ),
      onRatingUpdate: (val) {
        onRatingUpdate?.call(val);
      },
    );
  }
}
