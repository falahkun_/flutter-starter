// ignore_for_file: unused_import

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared/shared.dart';

class CustomInputDecoration extends Equatable {
  const CustomInputDecoration({
    this.errorBorderColor,
    this.enabledBorderColor,
    this.focusedBorderColor,
    this.backgroundColor,
    this.hintText,
    this.prefixIconPath,
    this.suffixWidget,
    this.labelText,
    this.hintStyle,
    this.suffixIcon,
    this.prefixIconColor,
    this.style,
    this.errorMessage,
    this.prefixWidget,
  });

  const CustomInputDecoration.empty()
      : errorBorderColor = null,
        enabledBorderColor = null,
        focusedBorderColor = null,
        backgroundColor = null,
        hintText = null,
        hintStyle = null,
        labelText = null,
        prefixIconPath = null,
        suffixWidget = null,
        suffixIcon = null,
        prefixIconColor = null,
        errorMessage = null,
        prefixWidget = null,
        style = null;

  final Color? errorBorderColor;
  final Color? enabledBorderColor;
  final Color? focusedBorderColor;
  final Color? backgroundColor;
  final String? hintText;
  final TextStyle? hintStyle;
  final TextStyle? style;
  final String? labelText;
  final SuffixIconWidget? suffixIcon;
  final String? errorMessage;

  /// pleasae fill using svg icon path
  final String? prefixIconPath;
  final Widget? suffixWidget;
  final Widget? prefixWidget;
  final Color? prefixIconColor;

  @override
  List<Object?> get props => [
        errorBorderColor,
        enabledBorderColor,
        focusedBorderColor,
        backgroundColor,
        hintText,
        hintStyle,
        style,
        labelText,
        prefixIconColor,
      ];
}

class SuffixIconWidget {
  SuffixIconWidget({
    required this.icon,
    required this.onPressed,
  });

  final IconData icon;
  final void Function() onPressed;
}

class CustomTextFormField extends StatefulWidget {
  const CustomTextFormField({
    required this.controller,
    super.key,
    this.validator,
    this.isError = false,
    this.obscureText = false,
    this.decoration,
    this.keyboardType,
    this.onChanged,
    this.removePaddingBottom = false,
    this.readOnly = false,
    this.onTap,
    this.maxLines,
    this.minLines,
    this.isMandatory = true,
    this.inputFormatters = const [],
    this.maxLengthEnforcement,
    this.maxLength,
  });

  final TextEditingController controller;
  final bool Function(String? value)? validator;
  final bool isError;
  final bool obscureText;
  final CustomInputDecoration? decoration;
  final TextInputType? keyboardType;
  final void Function(String? value)? onChanged;
  final bool removePaddingBottom;
  final bool readOnly;
  final void Function()? onTap;
  final int? maxLines;
  final int? minLines;
  final bool isMandatory;
  final MaxLengthEnforcement? maxLengthEnforcement;
  final List<TextInputFormatter> inputFormatters;
  final int? maxLength;

  @override
  State<CustomTextFormField> createState() => _CustomTextFormFieldState();

  static Widget buildIcon(String path, {double size = 24, Color? color}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 12, 8, 12),
      child: _buildIconFromSvg(path, size: size, color: color),
    );
  }

  static Widget _buildIconFromSvg(
    String path, {
    double size = 24,
    Color? color,
  }) {
    return SvgWidget(
      path,
      color: color,
      size: size,
    );
  }
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool isStateError = false;
  final FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_listener);
    focusNode.addListener(() {
      setState(() {});
    });
  }

  void _listener() {
    if (widget.validator != null) {
      if (widget.validator!(widget.controller.text)) {
        isStateError = false;
      } else {
        isStateError = true;
      }
      setState(() {});
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {});
  }

  @override
  void didUpdateWidget(covariant CustomTextFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  @override
  void reassemble() {
    setState(() {});
    super.reassemble();
  }

  @override
  void dispose() {
    focusNode.removeListener(() {
      setState(() {});
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (decoration.labelText != null)
          MandatoryLabelText(
            labelText: decoration.labelText!,
            style: CustomTextStyles.textMediumS.copyWith(
              color: const Color(0xFF0A0A0A),
              fontWeight: FontWeight.w500,
            ),
            isMandatory: widget.isMandatory,
          ),
        const SizedBox(
          height: 4,
        ),
        TextFormField(
          maxLines: widget.maxLines ?? 1,
          minLines: widget.minLines,
          onTap: widget.onTap,
          readOnly: widget.readOnly,
          onChanged: widget.onChanged,
          focusNode: focusNode,
          maxLength: widget.maxLength,
          style: CustomTextStyles.textRegularS
              .copyWith(color: Colors.black87)
              .merge(decoration.style),
          controller: widget.controller,
          obscureText: widget.obscureText,
          obscuringCharacter: '*',
          keyboardType: widget.keyboardType,
          maxLengthEnforcement: widget.maxLengthEnforcement,
          inputFormatters: widget.inputFormatters,
          decoration: InputDecoration(
            suffixIcon: decoration.suffixWidget ?? suffixIcon(),
            prefixIcon: decoration.prefixWidget ?? prefixWidget(),
            hintText: decoration.hintText,
            filled: decoration.backgroundColor != null,
            hintStyle: CustomTextStyles.textRegularS
                .copyWith(
                  color: Colors.black45,
                )
                .merge(decoration.hintStyle),
            border: isStateError || widget.isError
                ? errorBorder()
                : enabledBorder(),
            focusedBorder: isStateError || widget.isError
                ? errorBorder()
                : focusedBorder(),
            enabledBorder: isStateError || widget.isError
                ? errorBorder()
                : enabledBorder(),
            fillColor: decoration.backgroundColor,
            focusColor: focusColor,
          ),
        ),
        if (widget.isError && widget.decoration?.errorMessage != null) ...[
          6.ph(),
          Text(
            widget.decoration?.errorMessage ?? '',
            style: CustomTextStyles.textRegularS.copyWith(
              color: Colors.red,
            ),
          ),
        ],
        if (!widget.removePaddingBottom)
          const SizedBox(
            height: 16,
          ),
      ],
    );
  }

  Color get focusColor => decoration.focusedBorderColor ?? AppColors.primary;

  Color get errorColor => decoration.errorBorderColor ?? Colors.redAccent;

  Color get enableColor => decoration.enabledBorderColor ?? Colors.black45;

  Widget? suffixIcon() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        if (widget.isError)
          Icon(
            Icons.error_outline_outlined,
            color: errorColor,
          ),
        if (decoration.suffixIcon != null) ...[
          12.pw(),
          InkWell(
            onTap: decoration.suffixIcon!.onPressed,
            child: Icon(
              decoration.suffixIcon!.icon,
              color: isStateError || widget.isError
                  ? errorColor
                  : focusNode.hasFocus
                      ? focusColor
                      : enableColor,
            ),
          ),
        ],
        16.pw(),
      ],
    );
  }

  Widget? prefixWidget() {
    if (decoration.prefixIconPath != null) {
      return CustomTextFormField.buildIcon(
        decoration.prefixIconPath!,
        color: decoration.prefixIconColor ??
            (isStateError || widget.isError
                ? errorColor
                : focusNode.hasFocus
                    ? focusColor
                    : enableColor),
      );
    }
    return null;
  }

  CustomInputDecoration get decoration =>
      widget.decoration ?? const CustomInputDecoration.empty();

  InputBorder focusedBorder() => OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(color: focusColor, width: 2),
      );

  InputBorder errorBorder() => OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(color: errorColor, width: 2),
      );

  InputBorder enabledBorder() => OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(
          color: enableColor,
        ),
      );
}
