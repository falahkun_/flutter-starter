import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class BaseShimmer extends StatelessWidget {
  const BaseShimmer({
    super.key,
    this.child,
    this.height,
    this.width,
    this.radius,
  });

  final Widget? child;
  final double? height;
  final double? width;
  final BorderRadiusGeometry? radius;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: const Color(0xFFE0E0E0),
      highlightColor: const Color(0xFFF5F5F5),
      child: child ??
          Container(
            height: height,
            width: width,
            decoration: BoxDecoration(
              borderRadius: radius,
              color: Colors.grey,
            ),
          ),
    );
  }
}
