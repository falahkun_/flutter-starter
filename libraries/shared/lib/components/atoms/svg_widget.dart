import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared/shared.dart';

/// only supported asset file & network
class SvgWidget extends StatelessWidget {
  const SvgWidget(
    this.path, {
    super.key,
    this.size = 24,
    this.color,
  });

  final String path;
  final double size;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    if (path.contains('https://')) {
      return SvgPicture.network(
        path,
        height: size,
        width: size,
        colorFilter: color == null
            ? null
            : ColorFilter.mode(
                color ?? AppColors.primary,
                BlendMode.srcIn,
              ),
      );
    }
    return SvgPicture.asset(
      path,
      height: size,
      width: size,
      colorFilter: color == null
          ? null
          : ColorFilter.mode(
              color ?? AppColors.primary,
              BlendMode.srcIn,
            ),
    );
  }
}
