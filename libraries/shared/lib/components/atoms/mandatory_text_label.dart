
import 'package:flutter/material.dart';
import 'package:shared/shared.dart';

class MandatoryLabelText extends StatelessWidget {
  const MandatoryLabelText({
    required this.labelText, super.key,
    this.style,
    this.isMandatory = true,
  });

  final String labelText;
  final TextStyle? style;
  final bool isMandatory;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          labelText,
          style: style?.copyWith(
            color: AppColors.gray.shade900,
          ),
        ),
        if (isMandatory)
          Text(
            '*',
            style: mandatoryStyle(context, Colors.red.shade700),
          ),
      ],
    );
  }

  TextStyle? mandatoryStyle(
    BuildContext context,
    Color color, {
    bool isAsterisk = true,
  }) {
    if (style == null) {
      return Theme.of(context).textTheme.bodySmall?.copyWith(
            color: color,
          );
    }

    return style?.copyWith(
      color: color,
      fontWeight: isAsterisk ? FontWeight.normal : null,
    );
  }
}
