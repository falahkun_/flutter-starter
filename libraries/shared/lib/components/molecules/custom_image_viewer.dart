// ignore_for_file: prefer_null_aware_method_calls

import 'package:flutter/material.dart';

class CustomImageViewer extends StatelessWidget {
  const CustomImageViewer({
    required this.path,
    super.key,
    this.height,
    this.width,
    this.fit,
    this.onProgress,
  });

  final String path;
  final double? height;
  final double? width;
  final BoxFit? fit;
  final void Function(bool isLoading)? onProgress;

  @override
  Widget build(BuildContext context) {
    // if image is from network
    if (path.startsWith('http')) {
      return Image.network(
        path,
        fit: fit,
        height: height,
        width: width,
        errorBuilder: (_, err, stack) {
          return const SizedBox();
        },
        loadingBuilder: (_, image, progress) {
          if (onProgress != null) {
            onProgress!(progress != null);
          }
          if (progress != null && progress.expectedTotalBytes != null) {
            return Center(
              child: CircularProgressIndicator(
                value: progress.cumulativeBytesLoaded /
                    progress.expectedTotalBytes!,
              ),
            );
          } else {
            return image;
          }
        },
      );
    }
    return Image.asset(
      path,
      fit: fit,
      height: height,
      width: width,
      errorBuilder: (_, err, stack) {
        return const SizedBox();
      },
    );
  }
}
