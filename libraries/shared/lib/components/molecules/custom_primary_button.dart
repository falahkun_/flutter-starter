// ignore_for_file: tighten_type_of_initializing_formals

import 'package:flutter/material.dart';
import 'package:shared/shared.dart';

class CustomPrimaryButton extends StatelessWidget {
  const CustomPrimaryButton({
    required this.title,
    required this.onPressed,
    super.key,
    this.color,
    this.borderColor,
    this.width,
    this.height,
    this.enabled = true,
    this.elevation = false,
    this.disabledColor = Colors.black54,
  })  : labelStyle = null,
        labelText = null;

  const CustomPrimaryButton.label(
    this.labelText, {
    required this.onPressed,
    super.key,
    this.color,
    this.borderColor,
    this.width,
    this.height,
    this.enabled = true,
    this.elevation = false,
    this.disabledColor = Colors.black54,
    this.labelStyle,
  })  : title = null,
        assert(labelText != null, '[labelText] cannot be null');

  final Widget? title;
  final String? labelText;
  final void Function() onPressed;
  final Color? color;
  final Color? borderColor;
  final double? width;
  final double? height;
  final bool enabled;
  final bool elevation;
  final Color? disabledColor;
  final TextStyle? labelStyle;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: enabled ? onPressed : () {},
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 12,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: 8.r(),
        side: borderColor == null
            ? BorderSide.none
            : BorderSide(
                color: borderColor!,
              ),
      ),
      color:
          enabled ? color ?? AppColors.primary : disabledColor!.withOpacity(.2),
      splashColor: enabled ? Colors.black12 : Colors.transparent,
      highlightColor: enabled ? Colors.black12 : Colors.transparent,
      highlightElevation: elevation ? 1 : 0,
      elevation: elevation ? 1 : 0,
      minWidth: width,
      height: height,
      child: _buildChild(context),
    );
  }

  Widget _buildChild(BuildContext context) {
    if (title != null) {
      return title!;
    } else {
      return Text(
        labelText!,
        style: labelStyle ??
            CustomTextStyles.textBoldS.copyWith(
              color: enabled ? Colors.white : disabledColor,
            ),
      );
    }
  }
}
