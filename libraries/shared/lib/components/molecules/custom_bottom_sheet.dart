import 'package:core/core.dart';
import 'package:flutter/material.dart';
import 'package:shared/shared.dart';

typedef FunctionBuilder = Widget Function(
    BuildContext context, ScrollController controller);

class CustomBottomSheet extends StatefulWidget {
  const CustomBottomSheet({
    super.key,
    required this.builder,
    this.title,
    this.subtitle,
  });

  final FunctionBuilder builder;
  final String? title;
  final String? subtitle;

  @override
  State<CustomBottomSheet> createState() => _CustomBottomSheetState();
}

class _CustomBottomSheetState extends State<CustomBottomSheet> {
  final AppLogger logger = AppLogger(appName: 'CustomBottomSheet');

  @override
  Widget build(BuildContext context) {
    final max =
        (Dimens.height(context) - kToolbarHeight) / Dimens.height(context);
    return DraggableScrollableSheet(
      expand: false,
      maxChildSize: max,
      minChildSize: .3,
      initialChildSize: max,
      builder: (_, controller) {
        return DecoratedBox(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
            color: Colors.white,
          ),
          child: SafeArea(
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(
                    top: 8,
                    bottom: 12,
                  ),
                  width: 64,
                  height: 4,
                  decoration: BoxDecoration(
                    color: const Color(0xFFE0E0E0),
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                if (widget.title != null)
                  Container(
                    padding: const EdgeInsets.only(bottom: 4),
                    margin: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      widget.title!,
                      style: CustomTextStyles.textMediumHeaderXS,
                    ),
                  ),
                if (widget.subtitle != null)
                  Container(
                    padding: const EdgeInsets.only(bottom: 12),
                    margin: const EdgeInsets.fromLTRB(16, 0, 24, 12),
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: AppColors.gray.shade400,
                        ),
                      ),
                    ),
                    child: Text(
                      widget.subtitle!,
                      style: CustomTextStyles.textMediumS
                          .copyWith(color: AppColors.gray.shade700),
                    ),
                  ),
                Expanded(child: widget.builder(context, controller)),
              ],
            ),
          ),
        );
      },
    );
  }
}
