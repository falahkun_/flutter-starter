import 'package:flutter/material.dart';

class FadeAnimation extends StatefulWidget {
  const FadeAnimation({
    required this.delay,
    required this.child,
    super.key,
  });
  final double delay;
  final Widget child;

  @override
  State<FadeAnimation> createState() => _FadeAnimationState();
}

class _FadeAnimationState extends State<FadeAnimation>
    with TickerProviderStateMixin {
  late AnimationController animation;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    animation = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 800),
    );
    _animation = Tween<double>(begin: 0, end: 1).animate(animation);

    Future<void>.delayed(const Duration(milliseconds: 500))
        .then((value) => animation.forward());
  }

  @override
  void dispose() {
    super.dispose();
    animation.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (_, child) {
        return Opacity(
          opacity: _animation.value,
          child: widget.child,
        );
      },
      child: widget.child,
    );
  }
}

enum AniProps { translateY, opacity }
