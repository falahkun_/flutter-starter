import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:shared/preferences/preferences.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    required this.title, super.key,
    this.caption,
    this.height = kToolbarHeight,
    this.color,
    this.textColor,
    this.actions,
    this.bottom,
    this.systemUiOverlayStyle,
    this.leading,
  });

  final String title;
  final double height;
  final Color? color;
  final Color? textColor;
  final String? caption;
  final List<Widget>? actions;
  final PreferredSizeWidget? bottom;
  final SystemUiOverlayStyle? systemUiOverlayStyle;
  final Widget? leading;

  @override
  Size get preferredSize => Size.fromHeight(
        height + (bottom != null ? bottom!.preferredSize.height : 0),
      );

  @override
  Widget build(BuildContext context) {

    return AppBar(
      centerTitle: true,
      backgroundColor: color,
      elevation: 0,
      actions: actions,
      systemOverlayStyle: systemUiOverlayStyle,
      title: Text(
        title,
        style: CustomTextStyles.textMediumL.copyWith(
          fontWeight: FontWeight.w500,

        ),
      ),
      bottom: bottom,
      toolbarHeight: height,
      leading: leading ??
          IconButton(
            onPressed: () {
              if (context.canPop()) {
                context.pop();
              }
            },
            icon: const Icon(Icons.arrow_back_ios_new),
          ),
      titleTextStyle: TextStyle(
        color: textColor ?? Colors.white,
      ),
      iconTheme: IconThemeData(
        color: textColor ?? Colors.white,
        size: 16,
      ),
    );
  }
}
