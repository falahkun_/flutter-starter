import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:shared/shared.dart';

extension SnackbarMessage on BuildContext {
  Future<bool> showMessage(
    String title, {
    String? message,
  }) async {
    final data = MessageEntity(
      title: title.toMessage(this),
      message: message?.toMessage(this),
      context: this,
    );
    final result = await GetIt.I<SnackbarUseCase>().call(data);

    if (result.isRight()) {
      return true;
    }

    return false;
  }
}
