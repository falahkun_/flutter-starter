// coverage:ignore-file
import 'package:core/core.dart';
import 'package:dio/dio.dart';

extension DioErrorExtension on DioError {
  ServerException toServerException() {
    switch (type) {
      case DioErrorType.response:
        switch (response?.statusCode) {
          case 401:
            return UnAuthenticationServerException(
              message: _buildErrorMessage(response, message),
              code: response?.statusCode,
            );
          case 403:
            return UnAuthorizeServerException(
              message: _buildErrorMessage(response, message),
              code: response?.statusCode,
            );
          case 404:
            return NotFoundServerException(
              message: _buildErrorMessage(response, message),
              code: response?.statusCode,
            );
          case 500:
          case 502:
            return InternalServerException(
              message: _buildErrorMessage(
                response,
                'Ups server sedang penuh, coba ulangi sesaat lagi!',
              ),
              code: response?.statusCode,
            );
          default:
            return GeneralServerException(
              message: _buildErrorMessage(response, message),
              code: response?.statusCode,
            );
        }

      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        return TimeOutServerException(
          message: _buildErrorMessage(response, message),
          code: response?.statusCode,
        );

      case DioErrorType.cancel:
      case DioErrorType.other:
        return GeneralServerException(
          message: _buildErrorMessage(response, message),
          code: response?.statusCode,
        );
    }
  }
}

String _buildErrorMessage(Response<dynamic>? response, String message) {
  try {
    AppLogger().print(response?.data);
    final res = response!.data! as Map<String, dynamic>;
    final msg = res['message'] ?? res['error'];

    return  msg as String;
  } catch (err) {
    AppLogger().print(response?.data);

    if (message.contains('SocketException') ||
        message.contains('HandshakeException')) {
      return 'socket';
    }
    return message;
  }
}
