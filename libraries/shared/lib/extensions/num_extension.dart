import 'package:flutter/material.dart';

extension NumX on num {
  BorderRadius r() {
    return BorderRadius.circular(toDouble());
  }

  Radius rx() {
    return Radius.circular(toDouble());
  }

  SizedBox ph() {
    return SizedBox(
      height: toDouble(),
    );
  }

  SizedBox pw() {
    return SizedBox(
      width: toDouble(),
    );
  }
}
