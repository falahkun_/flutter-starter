import 'package:intl/intl.dart';

extension DateTimeX on DateTime {
  String toUtcFormatted() {
    return DateFormat('yyyy-MM-dd HH:mm:ss').format(toUtc());
  }

  String toLocalFormatted() {
    return DateFormat('yyyy-MM-dd HH:mm:ss').format(this);
  }

  String format([String? pattern, String? locale]) {
    return DateFormat(pattern ?? 'yyyy-MM-dd', locale ?? 'id').format(this);
  }
}
