import 'package:flutter/material.dart';
import 'package:shared/shared.dart';

extension StringX on String {
  AppTheme toAppTheme() {
    final appTheme =
        AppTheme.values.firstWhere((e) => e.toString() == 'AppTheme.$this');
    return appTheme;
  }

  String toMessage(BuildContext context) {
    final l10n = context.l10n;
    switch (errorMessagefromString(this)) {
      case ErrorMessage.ISSUE_NOT_FOUND:
      case ErrorMessage.socket:
      case ErrorMessage.USER_INACTIVE:
      case ErrorMessage.CREDENTIAL_INVALID:
      case ErrorMessage.UNSUPPORTED_FILE:
      case ErrorMessage.PERMISSION_DENIED:
      case ErrorMessage.CODE_ERROR:
      case ErrorMessage.CONTEXT_REQUIRED:
      case ErrorMessage.PLATFORM_NOT_SUPPORTED:
        return l10n.credentialEmpty;
      case ErrorMessage.EMAIL_INVALID:
        return l10n.emailInvalid;
      case null:
        return this;
    }
  }
}
