import 'package:intl/intl.dart';

extension ObjectExtension on Object {
  String? toStringDate() {
    final dateParser = DateFormat('dd MMM, yyyy');

    String? _parse(String date) {
      try {
        final reslt = DateTime.parse(date);
        return reslt.toIso8601String();
      } catch (err) {
        try {
          final result = dateParser.parse(date);
          return result.toIso8601String();
        } catch (err) {
          return null;
        }
      }
    }

    return _parse(toString());
  }
}