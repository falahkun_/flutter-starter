import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:home/home.dart';
import 'package:settings/settings.dart';
import 'package:shared/shared.dart';

// GoRouter configuration
final router = GoRouter(
  routes: [
    GoRoute(
      path: NamedRoutes.home,
      builder: (context, state) => const HomePage(),
      redirect: _middleware,
    ),
    GoRoute(
      path: NamedRoutes.settings,
      builder: (context, state) => const SettingsPage(),
    ),
    GoRoute(
      path: NamedRoutes.login,
      builder: (context, state) => const SignInPage(),
    ),
    GoRoute(
      path: NamedRoutes.register,
      builder: (context, state) => const RegisterPage(),
    ),
    GoRoute(
      path: NamedRoutes.forgotPassword,
      builder: (context, state) => const RequestForgotPasswordPage(),
    ),
  ],
);

Future<String?> _middleware(BuildContext context, GoRouterState state) async {
  try {
    final session = await GetIt.I<GetSessionUseCase>().call(const NoParams());
    // final data = session.toIterable().single;

    // AppLogger().print(data.toString());
    if (session.isRight()) {
      return null;
    }

    session.leftMap((l) => AppLogger().print(l.message));

    return NamedRoutes.login;
  } catch (err, stack) {
    AppLogger().print(err.toString(), stack);
    return NamedRoutes.login;
  }
}
