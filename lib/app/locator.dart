// ignore_for_file: cascade_invocations

import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:core/networks/dio_client.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:settings/settings.dart';
import 'package:shared/shared.dart';
import 'package:shared_preferences/shared_preferences.dart';

final getIt = GetIt.instance;

Future<void> setUpLocator({Flavor? flavor = Flavor.prod}) async {
  await _setUpCore(flavor!);

  /* -------------------------SETTINGS START-------------------------- */
  // data
  getIt.registerLazySingleton<SettingsLocalDataSource>(
    () => SettingsLocalDataSourceImpl(getIt()),
  );
  getIt.registerLazySingleton<SettingsRepository>(
    () => SettingsRepositoryImpl(getIt()),
  );

  // domain
  getIt.registerLazySingleton(() => GetDataSettingsUseCase(getIt()));
  getIt.registerLazySingleton(() => SetFlavorUseCase(getIt()));
  getIt.registerLazySingleton(() => SetLanguageUseCase(getIt()));
  getIt.registerLazySingleton(() => SetThemeUseCase(getIt()));

  // presentation
  getIt.registerFactory(
    () => SettingsBloc(
      getDataSettingsUseCase: getIt(),
      setFlavorUseCase: getIt(),
      setThemeUseCase: getIt(),
      setLanguageUseCase: getIt(),
    ),
  );
  getIt.registerFactory(LanguageBloc.new);

  /* --------------------------SETTINGS END--------------------------- */

  /* ---------------------------AUTH START--------------------------- */
  // data
  getIt.registerLazySingleton<LoginRemoteDataSource>(
    () => LoginRemoteDataSourceImpl(getIt()),
  );
  getIt.registerLazySingleton<AuthRemoteDataSource>(
    () => AuthRemoteDataSourceImpl(client: getIt()),
  );

  getIt.registerLazySingleton<LoginRepository>(
    () => LoginRepositoryImpl(getIt()),
  );
  getIt.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImpl(
      remoteDataSource: getIt(),
      localDataSource: getIt(),
    ),
  );

  // domain
  getIt.registerLazySingleton(() => SignInUseCase(getIt()));
  getIt.registerLazySingleton(() => LoginUseCase(repository: getIt()));
  getIt.registerLazySingleton(() => RegisterUseCase(repository: getIt()));
  getIt.registerLazySingleton(() => GetTokenUseCase(repository: getIt()));
  getIt.registerLazySingleton(() => GetRolesUseCase(repository: getIt()));
  getIt.registerLazySingleton(() => GetSessionUseCase(repository: getIt()));
  getIt.registerLazySingleton(
      () => GetForgotPasswordEmailUseCase(repository: getIt()));

  // presentation
  getIt.registerFactory(() => SignInBloc(signInUseCase: getIt()));
  getIt.registerFactory(() => RegisterBloc(getIt()));
  getIt.registerFactory(() => LoginBloc(getIt()));
  getIt.registerFactory(() => ForgotPasswordBloc(getIt()));
  getIt.registerFactory(CountryBloc.new);

  /* ----------------------------AUTH END---------------------------- */

  getIt.registerLazySingleton(RecordErrorUseCase.new);
}

Future<void> _setUpCore(Flavor flavor) async {
  F.flavor = flavor;
  final dio = Dio();
  final preferences = await SharedPreferences.getInstance();

  if (!kIsWeb) {
    final appDocDir = await getApplicationDocumentsDirectory();
    Hive.init('${appDocDir.path}/db');
  }
  getIt.registerLazySingleton<HiveInterface>(() => Hive);

  /* ------------------------------- AUTH START ------------------------------- */

  // Data
  final sessionLocalDataSource = SessionLocalDataSourceImpl(hive: getIt());
  getIt.registerLazySingleton<SessionLocalDataSource>(
    () => sessionLocalDataSource,
  );
  // Domain
  /* -------------------------------- AUTH END -------------------------------- */

  final token = await sessionLocalDataSource.getToken();

  getIt.registerLazySingleton(LocationHelper.new);

  getIt
    ..registerLazySingleton<Dio>(() => DioClient(dio, token: token).client)
    ..registerLazySingleton(() => preferences)
    ..registerLazySingleton(() => BaseRequest(getIt()));

  getIt.registerLazySingleton(SnackbarHelper.new);
  getIt.registerLazySingleton(
    () => SnackbarUseCase(
      helper: getIt(),
    ),
  );
}
