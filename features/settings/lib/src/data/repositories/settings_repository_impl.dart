import 'package:core/core.dart';
import 'package:dartz/dartz.dart';
import 'package:settings/settings.dart';
import 'package:shared/preferences/themes/enums.dart';

class SettingsRepositoryImpl implements SettingsRepository {
  SettingsRepositoryImpl(this.settingsLocalDataSource);

  final SettingsLocalDataSource settingsLocalDataSource;

  @override
  Future<Either<Failure, bool>> setFlavor(Flavor flavor) async {
    try {
      final result = await settingsLocalDataSource.setFlavor(flavor);
      return Right(result);
    } on CacheException catch (err) {
      return Left(
        CacheFailure(
          message: err.message,
          code: err.code,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, bool>> setLanguage(Language language) async {
    try {
      final result = await settingsLocalDataSource.setLanguage(language);
      return Right(result);
    } on CacheException catch (err) {
      return Left(
        CacheFailure(
          message: err.message,
          code: err.code,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, bool>> setTheme(AppTheme theme) async {
    try {
      final result = await settingsLocalDataSource.setTheme(theme);
      return Right(result);
    } on CacheException catch (err) {
      return Left(
        CacheFailure(
          message: err.message,
          code: err.code,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, Settings>> getData() async {
    try {
      final result = await settingsLocalDataSource.getData();
      return Right(result);
    } on CacheException catch (err) {
      return Left(
        CacheFailure(
          message: err.message,
          code: err.code,
        ),
      );
    }
  }
}
