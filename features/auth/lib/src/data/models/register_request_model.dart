import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'register_request_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class RegisterRequestModel extends Equatable {
  const RegisterRequestModel({
    required this.name,
    required this.lastName,
    required this.email,
    required this.identityCardNumber,
    required this.terms,
    required this.password,
    required this.passwordConfirmation,
  });

  factory RegisterRequestModel.fromJson(Map<String, dynamic> json) =>
      _$RegisterRequestModelFromJson(json);

  final String name;
  final String lastName;
  final String email;
  @JsonKey(name: 'nik')
  final String? identityCardNumber;
  final int terms;
  final String password;
  final String passwordConfirmation;

  Map<String, dynamic> toJson() => _$RegisterRequestModelToJson(this);

  @override
  List<Object?> get props => [
        name,
        lastName,
        email,
        identityCardNumber,
        terms,
        password,
        passwordConfirmation,
      ];
}
