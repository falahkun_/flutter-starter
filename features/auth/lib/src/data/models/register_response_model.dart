/**
 * {
    "id": 50,
    "full_name": "Yose Takahara",
    "name": "Yose",
    "last_name": "Takahara",
    "email": "falath.work@gmail.com",
    "confirmed": false,
    "roles": [
    {
    "name": "member",
    "label": "Member"
    }
    ],
    "job_title": null,
    "picture": "https://app.kotapedia.id/assets/corals/images/avatars/avatar_0.png",
    "picture_thumb": "https://app.kotapedia.id/assets/corals/images/avatars/avatar_0.png",
    "about": null,
    "phone_country_code": "",
    "phone_number": "",
    "address": null,
    "integration_id": null,
    "gateway": null,
    "card_brand": null,
    "card_last_four": null,
    "payment_method_token": null,
    "created_at": "06 Jan, 2024",
    "updated_at": "06 Jan, 2024",
    "authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYjI0Yjk1MWFiODU4M2VhNThhZDE1OTg1YTk0NzllMDFkZmE3MjQ1ZDkyYzY5ZWQ2YTIzYWQ2NTZkZmFkNTAwOGMzYjM5YjU1M2I4NDZjYTIiLCJpYXQiOjE3MDQ1MTM4NzguNTU1NTE2LCJuYmYiOjE3MDQ1MTM4NzguNTU1NTIsImV4cCI6MTczNjEzNjI3OC41NDc0ODksInN1YiI6IjUwIiwic2NvcGVzIjpbXX0.QuBn5Z303bMbCpc5peRamdFs5kwTws4oG5IhxZyAPnMGHrnfBtVmtCurCd-ZCT6CsZUICqrtzFZAwWk0V7WGAGhQhR4IXT_lH4Fhqq7PfVt5Z1SO0M2PQQHnj95IgTAak_KkIRw0_D7MFPV_fzGf3FvSYViu9r2_9GG0YrhezcXFEVEzd2vQj806pkDTSX8MX-ia60jN1o4duZ9kUVluGnFia5FInlVx3QzElRVfL0vjyK_d_rIioA8etj9ljyfbWtWWko-4Eyz3ntYzttyv2AM9Ms2EWQMWE5R-Ep6GtFmhreE6eGfFYjPU1hiJWHE2aPT67zGo8ax3V-hYiZQYrttojr53j9k0AIkLWsw1vg6aH_UTD6uJZMMap1GUdcbVdA7PItmFwd9KxYhKH1VvIi4dPb_Uq_QJET76MwvPWu9mAwWse2mMtFGZnp-vcS4QF-ULEmi1FC59IWElNKUznMPEQ6mvtlkSujKYjkzZqVTV8ogDgV1Tg8xH98DXQZTC8qdtrnAG43q2gNEaaMnZueewALAc-_M7J7SNxg7eOlJ4Z9KfG5Vs45A2hH_n3E8WfJ3UtNNKRKL_UyLfk-TFg_Gh7Vim8QcK2KnIoLIZe3Tf3OSnxO_YwhimZ7ApLmayuP0MH5sQp-ZOzUzKlj7VBEBo2n3cgezZ13OihFN-Sfo",
    "expires_at": "2025-01-06 11:04:38"
    }
 */

import 'package:auth/auth.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:shared/shared.dart';

part 'register_response_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class RegisterResponseModel extends Equatable {
  const RegisterResponseModel({
    required this.id,
    required this.fullName,
    required this.name,
    required this.email,
    required this.confirmed,
    required this.roles,
    required this.jobTitle,
    required this.picture,
    required this.pictureTumb,
    required this.about,
    required this.phoneCountryCode,
    required this.phoneNumber,
    required this.address,
    required this.integrationId,
    required this.gateway,
    required this.cardBrand,
    required this.cardLastFour,
    required this.paymentMethodToken,
    required this.createdAt,
    required this.updatedAt,
    required this.authorization,
    required this.expiresAt,
  });

  factory RegisterResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['created_at'] != null) {
      json['created_at'] = json['created_at'].toString().toStringDate();
    }

    if (json['updated_at'] != null) {
      json['updated_at'] = json['updated_at'].toString().toStringDate();
    }
    return _$RegisterResponseModelFromJson(json);
  }

  final int id;
  final String fullName;
  final String name;
  final String email;
  final bool confirmed;
  final List<RolesModel> roles;
  final String? jobTitle;
  final String? picture;
  final String? pictureTumb;
  final String? about;
  final String? phoneCountryCode;
  final String? phoneNumber;
  final String? address;
  final dynamic integrationId;
  final dynamic gateway;
  final dynamic cardBrand;
  final dynamic cardLastFour;
  final dynamic paymentMethodToken;
  final DateTime createdAt;
  final DateTime? updatedAt;
  final String authorization;
  final DateTime expiresAt;

  Map<String, dynamic> toJson() => _$RegisterResponseModelToJson(this);

  AuthEntity toEntity() => AuthEntity(
        id: id,
        fullName: fullName,
        name: name,
        email: email,
        confirmed: confirmed,
        roles: roles,
        jobTitle: jobTitle,
        picture: picture,
        pictureTumb: pictureTumb,
        about: about,
        phoneCountryCode: phoneCountryCode,
        phoneNumber: phoneNumber,
        address: address,
        createdAt: createdAt,
        updatedAt: updatedAt,
      );

  @override
  List<Object?> get props => [
        id,
        fullName,
        name,
        email,
        confirmed,
        roles,
        jobTitle,
        picture,
        pictureTumb,
        about,
        phoneCountryCode,
        phoneNumber,
        address,
        integrationId,
        gateway,
        cardBrand,
        cardLastFour,
        paymentMethodToken,
        createdAt,
        updatedAt,
        authorization,
        expiresAt,
      ];
}
