// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterRequestModel _$RegisterRequestModelFromJson(
        Map<String, dynamic> json) =>
    RegisterRequestModel(
      name: json['name'] as String,
      lastName: json['last_name'] as String,
      email: json['email'] as String,
      identityCardNumber: json['nik'] as String?,
      terms: json['terms'] as int,
      password: json['password'] as String,
      passwordConfirmation: json['password_confirmation'] as String,
    );

Map<String, dynamic> _$RegisterRequestModelToJson(
        RegisterRequestModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'last_name': instance.lastName,
      'email': instance.email,
      'nik': instance.identityCardNumber,
      'terms': instance.terms,
      'password': instance.password,
      'password_confirmation': instance.passwordConfirmation,
    };
