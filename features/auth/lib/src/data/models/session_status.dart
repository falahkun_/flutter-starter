import 'package:equatable/equatable.dart';

class SessionStatus extends Equatable {
  const SessionStatus({
    required this.status,
    required this.message,
  });

  static SessionStatus get expired => const SessionStatus(
        message: 'SESSION_EXPIRED',
        status: false,
      );

  final bool status;
  final String? message;

  @override
  List<Object?> get props => [
        status,
        message,
      ];
}
