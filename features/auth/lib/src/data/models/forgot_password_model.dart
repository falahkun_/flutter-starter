import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'forgot_password_model.g.dart';

@JsonSerializable(
  fieldRename: FieldRename.snake,
  createFactory: false,
)
class ForgotPasswordModel extends Equatable {
  const ForgotPasswordModel({required this.email});

  final String email;

  Map<String, dynamic> toJson() => _$ForgotPasswordModelToJson(this);

  @override
  List<Object?> get props => [
        email,
      ];
}
