// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterResponseModel _$RegisterResponseModelFromJson(
        Map<String, dynamic> json) =>
    RegisterResponseModel(
      id: json['id'] as int,
      fullName: json['full_name'] as String,
      name: json['name'] as String,
      email: json['email'] as String,
      confirmed: json['confirmed'] as bool,
      roles: (json['roles'] as List<dynamic>)
          .map((e) => RolesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      jobTitle: json['job_title'] as String?,
      picture: json['picture'] as String?,
      pictureTumb: json['picture_tumb'] as String?,
      about: json['about'] as String?,
      phoneCountryCode: json['phone_country_code'] as String?,
      phoneNumber: json['phone_number'] as String?,
      address: json['address'] as String?,
      integrationId: json['integration_id'],
      gateway: json['gateway'],
      cardBrand: json['card_brand'],
      cardLastFour: json['card_last_four'],
      paymentMethodToken: json['payment_method_token'],
      createdAt: DateTime.parse(json['created_at'] as String),
      updatedAt: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
      authorization: json['authorization'] as String,
      expiresAt: DateTime.parse(json['expires_at'] as String),
    );

Map<String, dynamic> _$RegisterResponseModelToJson(
        RegisterResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'full_name': instance.fullName,
      'name': instance.name,
      'email': instance.email,
      'confirmed': instance.confirmed,
      'roles': instance.roles,
      'job_title': instance.jobTitle,
      'picture': instance.picture,
      'picture_tumb': instance.pictureTumb,
      'about': instance.about,
      'phone_country_code': instance.phoneCountryCode,
      'phone_number': instance.phoneNumber,
      'address': instance.address,
      'integration_id': instance.integrationId,
      'gateway': instance.gateway,
      'card_brand': instance.cardBrand,
      'card_last_four': instance.cardLastFour,
      'payment_method_token': instance.paymentMethodToken,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'authorization': instance.authorization,
      'expires_at': instance.expiresAt.toIso8601String(),
    };
