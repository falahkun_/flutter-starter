import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'login_data_model.g.dart';

@JsonSerializable()
class LoginDataModel extends Equatable {
  const LoginDataModel({
    required this.token,
  });

  factory LoginDataModel.fromJson(Map<String, dynamic> json) =>
      _$LoginDataModelFromJson(json);

  final String token;

  Map<String, dynamic> toJson() => _$LoginDataModelToJson(this);

  @override
  List<Object?> get props => [token];
}
