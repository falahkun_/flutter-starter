import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';

class LoginRepositoryImpl implements LoginRepository {
  LoginRepositoryImpl(this.remoteDataSource);

  final LoginRemoteDataSource remoteDataSource;

  @override
  Future<Either<Failure, String>> signIn(LoginRequestModel requestData) async {
    try {
      final result = await remoteDataSource.signIn(requestData);
      if (!result.status) {
        throw GeneralServerException(message: result.message);
      }
      return Right(result.data.token);
    } on GeneralServerException catch (err) {
      return Left(ServerFailure(message: err.message, code: err.code));
    } on ErrorCodeException catch (err) {
      return Left(CodeFailure(message: err.message));
    }
  }
}
