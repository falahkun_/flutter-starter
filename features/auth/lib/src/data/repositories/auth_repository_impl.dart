import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';

class AuthRepositoryImpl implements AuthRepository {
  AuthRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
  });

  final AuthRemoteDataSource remoteDataSource;
  final SessionLocalDataSource localDataSource;

  @override
  Future<Either<Failure, String>> getRoles() async {
    try {
      final result = await localDataSource.getRoles();
      return Right(result);
    } on GeneralServerException catch (err) {
      return Left(ServerFailure(message: err.message));
    } on ErrorCodeException catch (err) {
      return Left(CodeFailure(message: err.message));
    } on CacheException catch (err) {
      return Left(CacheFailure(message: err.message));
    } catch (err) {
      return const Left(CodeFailure(message: 'CODE_ERROR'));
    }
  }

  @override
  Future<Either<Failure, String>> getToken() async {
    try {
      final result = await localDataSource.getToken();
      return Right(result);
    } on GeneralServerException catch (err) {
      return Left(ServerFailure(message: err.message));
    } on ErrorCodeException catch (err) {
      return Left(CodeFailure(message: err.message));
    } on CacheException catch (err) {
      return Left(CacheFailure(message: err.message));
    } catch (err) {
      return const Left(CodeFailure(message: 'CODE_ERROR'));
    }
  }

  @override
  Future<Either<Failure, AuthEntity>> getUser() async {
    try {
      final result = await localDataSource.getData();
      return Right(result.toEntity());
    } on GeneralServerException catch (err) {
      return Left(ServerFailure(message: err.message));
    } on ErrorCodeException catch (err, stack) {
      AppLogger().print(err.toString(), stack);
      return Left(CodeFailure(message: err.message));
    } on CacheException catch (err) {
      return Left(CacheFailure(message: err.message));
    } catch (err) {
      return const Left(CodeFailure(message: 'CODE_ERROR'));
    }
  }

  @override
  Future<Either<Failure, AuthEntity>> login(LoginRequestModel request) async {
    try {
      final result = await remoteDataSource.login(request);
      await localDataSource.saveCache(result);
      
      return Right(result.toEntity());
    } on GeneralServerException catch (err) {
      return Left(ServerFailure(message: err.message));
    } on ErrorCodeException catch (err) {
      return Left(CodeFailure(message: err.message));
    } on CacheException catch (err) {
      return Left(CacheFailure(message: err.message));
    } catch (err) {
      AppLogger().print('Error($err)');
      return const Left(CodeFailure(message: 'CODE_ERROR'));
    }
  }

  @override
  Future<Either<Failure, AuthEntity>> register(
    RegisterRequestModel request,
  ) async {
    try {
      final result = await remoteDataSource.register(request);
      await localDataSource.saveCache(result);
      return Right(result.toEntity());
    } on GeneralServerException catch (err) {
      return Left(ServerFailure(message: err.message));
    } on ErrorCodeException catch (err) {
      return Left(CodeFailure(message: err.message));
    } on CacheException catch (err) {
      return Left(CacheFailure(message: err.message));
    } catch (err) {
      return const Left(CodeFailure(message: 'CODE_ERROR'));
    }
  }

  @override
  Future<Either<Failure, SessionStatus>> getSessionStatus() async {
    try {
      final result = await localDataSource.checkSession();
      return Right(result);
    } on GeneralServerException catch (err) {
      return Left(ServerFailure(message: err.message));
    } on ErrorCodeException catch (err) {
      return Left(CodeFailure(message: err.message));
    } on CacheException catch (err) {
      return Left(CacheFailure(message: err.message));
    } catch (err) {
      return const Left(CodeFailure(message: 'CODE_ERROR'));
    }
  }

  @override
  Future<Either<Failure, bool>> forgotPassword(
    ForgotPasswordModel request,
  ) async {
    try {
      final result = await remoteDataSource.forgotPassword(request);
      return Right(result);
    } on GeneralServerException catch (err) {
      return Left(ServerFailure(message: err.message));
    } on ErrorCodeException catch (err) {
      return Left(CodeFailure(message: err.message));
    } on CacheException catch (err) {
      return Left(CacheFailure(message: err.message));
    } catch (err) {
      return const Left(CodeFailure(message: 'CODE_ERROR'));
    }
  }
}
