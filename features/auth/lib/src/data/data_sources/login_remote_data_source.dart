import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dio/dio.dart';
import 'package:shared/shared.dart';

mixin LoginRemoteDataSource {
  Future<LoginResponseModel> signIn(LoginRequestModel requestData);
}

class LoginRemoteDataSourceImpl implements LoginRemoteDataSource {
  LoginRemoteDataSourceImpl(this.dio);

  final Dio dio;

  @override
  Future<LoginResponseModel> signIn(LoginRequestModel requestData) async {
    try {
      final response = await dio.post<Map<String, dynamic>>(
        '/v1/auth/',
        data: requestData.toJson(),
      );

      final data = response.data!;
      return LoginResponseModel.fromJson(data);
    } on DioError catch (err) {
      final exception = err.toServerException();
      throw GeneralServerException(
        message: exception.message,
        code: exception.code,
      );
    } catch (err) {
      throw ErrorCodeException(
        message: 'Oops Something errors!, message: $err',
        code: 500,
      );
    }
  }
}
