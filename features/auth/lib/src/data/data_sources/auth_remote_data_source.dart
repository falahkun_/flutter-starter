import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:shared/shared.dart';

mixin AuthRemoteDataSource {
  Future<RegisterResponseModel> login(LoginRequestModel request);
  Future<RegisterResponseModel> register(RegisterRequestModel request);
  Future<bool> forgotPassword(ForgotPasswordModel request);
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  AuthRemoteDataSourceImpl({required this.client});

  final BaseRequest client;
  @override
  Future<RegisterResponseModel> login(LoginRequestModel request) async {
    final result = await client.post(
      ApiPath.authLogin,
      data: request.toJson(),
    );
    final data = result!['data'];
    return RegisterResponseModel.fromJson(data);
  }

  @override
  Future<RegisterResponseModel> register(RegisterRequestModel request) async {
    final result = await client.post(
      ApiPath.authRegister,
      data: request.toJson(),
    );
    final data = result!['data'];
    return RegisterResponseModel.fromJson(data);
  }

  @override
  Future<bool> forgotPassword(ForgotPasswordModel request) async {
    final result = await client.post(
      ApiPath.authForgotPassword,
      data: request.toJson(),
    );
    final data = result!['status'];
    return data == 'success';
  }
}
