import 'dart:convert';
import 'dart:developer';

import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:hive/hive.dart';

mixin SessionLocalDataSource implements CacheDataSource<RegisterResponseModel> {
  Future<SessionStatus> checkSession();
  Future<String> getToken();

  /// becase response from api is List Item
  /// the return "role_1, role_2"
  Future<String> getRoles();
}

class SessionLocalDataSourceImpl implements SessionLocalDataSource {
  SessionLocalDataSourceImpl({required this.hive});

  final HiveInterface hive;
  @override
  String cacheKey = 'session_key';

  @override
  Future<SessionStatus> checkSession() async {
    final data = await getData();

    final isExpired = data.expiresAt.isAfter(DateTime.now());
    if (isExpired) {
      return SessionStatus.expired;
    }

    return const SessionStatus(
      status: true,
      message: null,
    );
  }

  @override
  Future<bool> clearCache() async {
    try {
      final box = await _getBox();
      await box?.clear();

      return true;
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }

  @override
  Future<RegisterResponseModel> getData() async {
    try {
      final box = await _getBox();
      final boxData = box?.get('data');
      if (boxData != null && boxData is String) {
        return RegisterResponseModel.fromJson(
          json.decode(boxData) as Map<String, dynamic>,
        );
      }

      throw const NotFoundCacheException(message: 'Cache Not Found');
    } catch (e, stack) {
      log(e.toString(), error: e, stackTrace: stack);
      throw CacheException(message: e.toString());
    }
  }

  @override
  Future<String> getRoles() async {
    final data = await getData();
    return data.roles.map((e) => e.label).join(', ');
  }

  @override
  Future<String> getToken() async {
    try {
      final data = await getData();
      var authorization = data.authorization;
      if (authorization.startsWith('Bearer')) {
        authorization = authorization.split(' ').last;
      }

      return authorization;
    } catch (err) {
      return '';
    }
  }

  @override
  Future<bool> isCached() async {
    try {
      final box = await _getBox();

      return box?.get('data') != null;
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }

  @override
  Future<bool> saveCache(RegisterResponseModel data) async {
    try {
      final box = await _getBox();

      await box?.put(
        'data',
        json.encode(data),
      );

      return true;
    } catch (e) {
      throw CacheException(message: e.toString());
    }
  }

  Future<Box<Object?>?> _getBox() async {
    if (!hive.isBoxOpen(cacheKey)) {
      return hive.openBox(cacheKey);
    }

    return hive.box(cacheKey);
  }
}
