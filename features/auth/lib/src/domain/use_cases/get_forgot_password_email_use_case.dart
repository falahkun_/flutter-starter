import 'dart:async';

import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';

class GetForgotPasswordEmailUseCase
    implements UseCaseFuture<Failure, bool, ForgotPasswordModel> {
  GetForgotPasswordEmailUseCase({required this.repository});

  final AuthRepository repository;
  @override
  FutureOr<Either<Failure, bool>> call(ForgotPasswordModel params) async =>
      await repository.forgotPassword(params);
}
