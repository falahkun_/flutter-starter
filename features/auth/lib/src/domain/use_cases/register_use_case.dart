import 'dart:async';

import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';

class RegisterUseCase
    implements UseCaseFuture<Failure, AuthEntity, RegisterRequestModel> {
  RegisterUseCase({required this.repository});

  final AuthRepository repository;
  @override
  FutureOr<Either<Failure, AuthEntity>> call(
    RegisterRequestModel params,
  ) async =>
      await repository.register(params);
}
