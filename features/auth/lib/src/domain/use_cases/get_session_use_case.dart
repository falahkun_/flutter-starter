import 'dart:async';

import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';
import 'package:shared/shared.dart';

class GetSessionUseCase
    implements UseCaseFuture<Failure, SessionStatus, NoParams> {
  GetSessionUseCase({required this.repository});

  final AuthRepository repository;
  @override
  FutureOr<Either<Failure, SessionStatus>> call(
    NoParams params,
  ) async =>
      await repository.getSessionStatus();
}
