import 'dart:async';

import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';

class LoginUseCase
    implements UseCaseFuture<Failure, AuthEntity, LoginRequestModel> {
  LoginUseCase({required this.repository});

  final AuthRepository repository;
  @override
  FutureOr<Either<Failure, AuthEntity>> call(
    LoginRequestModel params,
  ) async =>
      await repository.login(params);
}
