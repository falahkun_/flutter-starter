import 'dart:async';

import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';
import 'package:shared/shared.dart';

class GetRolesUseCase implements UseCaseFuture<Failure, String, NoParams> {
  GetRolesUseCase({required this.repository});

  final AuthRepository repository;
  @override
  FutureOr<Either<Failure, String>> call(
    NoParams params,
  ) async =>
      await repository.getRoles();
}
