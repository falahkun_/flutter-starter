import 'dart:async';

import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';

class SignInUseCase
    implements UseCaseFuture<Failure, String, LoginRequestModel> {
  SignInUseCase(this.repository);

  final LoginRepository repository;

  @override
  FutureOr<Either<Failure, String>> call(LoginRequestModel params) async =>
      repository.signIn(params);
}
