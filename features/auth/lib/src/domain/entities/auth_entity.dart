import 'package:equatable/equatable.dart';
import 'package:shared/shared.dart';

class AuthEntity extends Equatable {
  const AuthEntity({
    required this.id,
    required this.fullName,
    required this.name,
    required this.email,
    required this.confirmed,
    required this.roles,
    required this.jobTitle,
    required this.picture,
    required this.pictureTumb,
    required this.about,
    required this.phoneCountryCode,
    required this.phoneNumber,
    required this.address,
    required this.createdAt,
    required this.updatedAt,
  });

  final int id;
  final String fullName;
  final String name;
  final String email;
  final bool confirmed;
  final List<RolesModel> roles;
  final String? jobTitle;
  final String? picture;
  final String? pictureTumb;
  final String? about;
  final String? phoneCountryCode;
  final String? phoneNumber;
  final String? address;
  final DateTime createdAt;
  final DateTime? updatedAt;

  @override
  List<Object?> get props => [
        id,
        fullName,
        name,
        email,
        confirmed,
        roles,
        jobTitle,
        picture,
        pictureTumb,
        about,
        phoneCountryCode,
        phoneNumber,
        address,
        createdAt,
        updatedAt,
      ];
}
