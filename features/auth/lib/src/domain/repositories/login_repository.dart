import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';

mixin LoginRepository {
  Future<Either<Failure, String>> signIn(LoginRequestModel requestData);
}
