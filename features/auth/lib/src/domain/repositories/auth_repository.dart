import 'package:auth/auth.dart';
import 'package:core/core.dart';
import 'package:dartz/dartz.dart';

mixin AuthRepository {
  Future<Either<Failure, AuthEntity>> login(LoginRequestModel request);
  Future<Either<Failure, AuthEntity>> register(RegisterRequestModel request);
  Future<Either<Failure, bool>> forgotPassword(ForgotPasswordModel request);
  Future<Either<Failure, SessionStatus>> getSessionStatus();
  Future<Either<Failure, String>> getToken();
  Future<Either<Failure, String>> getRoles();
  Future<Either<Failure, AuthEntity>> getUser();
}
