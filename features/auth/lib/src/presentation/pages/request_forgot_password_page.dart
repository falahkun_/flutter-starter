import 'package:auth/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared/shared.dart';

class RequestForgotPasswordPage extends StatefulWidget {
  const RequestForgotPasswordPage({super.key});

  @override
  State<RequestForgotPasswordPage> createState() => _RequestForgotPasswordPageState();
}

class _RequestForgotPasswordPageState extends State<RequestForgotPasswordPage> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
      listener: (_, state) {
        if (state is ForgotPasswordLoading) {
          AppLoading.show(context);
        } else if (state is ForgotPasswordError) {
          AppLoading.hide();
        } else if (state is ForgotPasswordSuccess) {
          AppLoading.hide();
        }
      },
      child: const BaseAuthScreen(body: _Body()),
    );
  }
}

class _Body extends StatefulWidget {
  const _Body();

  @override
  State<_Body> createState() => __BodyState();
}

class __BodyState extends State<_Body> {
  final emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            context.l10n.forgotPassword,
            style: CustomTextStyles.textSemiboldM.copyWith(
              color: AppColors.gray.shade900,
            ),
          ),
          12.ph(),
          Text(
            context.l10n.requestEmailCaption,
            style: CustomTextStyles.textRegularS.copyWith(
              color: AppColors.gray.shade900,
            ),
          ),
          24.ph(),
          CustomTextFormField(
            controller: emailController,
            keyboardType: TextInputType.emailAddress,
            validator: (val) => EmailValidator.validate(val ?? ''),
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.email,
              hintText: 'example@mail.com',
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
            ),
          ),
          CustomPrimaryButton.label(
            context.l10n.send,
            onPressed: () {
              final requestData = ForgotPasswordModel(
                email: emailController.text.trim(),
              );

              context
                  .read<ForgotPasswordBloc>()
                  .add(GetEmailVerificationEvent(requestData: requestData));
            },
            width: double.maxFinite,
          ),
        ],
      ),
    );
  }
}
