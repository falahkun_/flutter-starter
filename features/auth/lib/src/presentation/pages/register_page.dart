import 'package:auth/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared/shared.dart';
import 'package:go_router/go_router.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return  BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is RegisterLoading) {
          AppLoading.show(context);
        } else if (state is RegisterError) {
          AppLoading.hide();
          context.showMessage(state.message);
        } else if (state is RegisterSuccess) {
          AppLoading.hide();
          context.go(NamedRoutes.home);
        }
      },
      child: const BaseAuthScreen(
        body: _Body(),
      ),
    );
  }
}

class _Body extends StatefulWidget {
  const _Body();

  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final passwordConfirmController = TextEditingController();
  final nameController = TextEditingController();
  final lastNameController = TextEditingController();
  final phoneNumberController = TextEditingController();

  final countryCodeController = ValueNotifier(CountryCode.countries.first);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: ListView(
        children: [
          CustomTextFormField(
            controller: nameController,
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.name,
              hintText: 'Username 123',
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
            ),
          ),
          CustomTextFormField(
            controller: lastNameController,
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.lastName,
              hintText: 'Username 123',
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
            ),
          ),
          CustomTextFormField(
            controller: emailController,
            keyboardType: TextInputType.emailAddress,
            validator: (val) => EmailValidator.validate(val ?? ''),
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.email,
              hintText: 'example@mail.com',
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
            ),
          ),
          CustomTextFormField(
            controller: phoneNumberController,
            keyboardType: TextInputType.phone,
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.phoneNumber,
              hintText: '8123456',
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
              prefixWidget: SelectCoutryCode(
                onChanged: (country) {
                  setState(() {
                    countryCodeController.value = country;
                  });

                  Navigator.pop(context);
                },
                defaultValue: countryCodeController.value,
              ),
            ),
          ),
          CustomTextFormField(
            controller: passwordController,
            obscureText: true,
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.password,
              hintText: '********',
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
            ),
            removePaddingBottom: true,
          ),
          CustomTextFormField(
            controller: passwordConfirmController,
            obscureText: true,
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.confirmationValue(context.l10n.password),
              hintText: '********',
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
            ),
            removePaddingBottom: true,
          ),
          24.ph(),
          CustomPrimaryButton.label(
            context.l10n.register,
            onPressed: () {
              final requestData = RegisterRequestModel(
                name: nameController.text.trim(),
                lastName: lastNameController.text.trim(),
                email: emailController.text,
                identityCardNumber: '',
                terms: 1,
                password: passwordController.text.trim(),
                passwordConfirmation: passwordConfirmController.text.trim(),
              );

              context.read<RegisterBloc>().add(RegisterEvent(requestData));
            },
            width: double.maxFinite,
          ),
          32.ph(),
          const _LoginButton(),
        ],
      ),
    );
  }
}

class _LoginButton extends StatelessWidget {
  const _LoginButton();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          context.l10n.alreadyHaveAccount,
          style: CustomTextStyles.textRegularS.copyWith(
            color: AppColors.gray.shade900,
          ),
        ),
        TextButton(
          onPressed: () {
            if (context.canPop()) {
              context.pop();
            } else {
              context.go(NamedRoutes.register);
            }
          },
          child: Text(
            context.l10n.login,
            style: CustomTextStyles.textMediumS.copyWith(
              color: AppColors.primary,
            ),
          ),
        ),
      ],
    );
  }
}

class SelectCoutryCode extends StatefulWidget {
  const SelectCoutryCode({
    required this.onChanged,
    required this.defaultValue,
    super.key,
  });

  final void Function(CountryCode country) onChanged;
  final CountryCode defaultValue;

  @override
  State<SelectCoutryCode> createState() => _SelectCoutryCodeState();
}

class _SelectCoutryCodeState extends State<SelectCoutryCode> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showCountryCodeSelector(context);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              widget.defaultValue.flag,
              height: 20,
              width: 20,
            ),
            8.pw(),
            Text(
              '+${widget.defaultValue.phoneCode.toUpperCase()}',
              style: CustomTextStyles.textRegularXS.copyWith(
                color: AppColors.gray.shade900,
              ),
            ),
            4.pw(),
            Icon(
              Icons.arrow_drop_down,
              color: AppColors.gray.shade400,
            ),
            Container(
              height: 30,
              width: 1,
              color: AppColors.gray.shade600,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> showCountryCodeSelector(BuildContext context) async {
    await showModalBottomSheet<void>(
      context: context,
      builder: (_) {
        return BlocBuilder<CountryBloc, List<CountryCode>>(
          builder: (context, state) {
            return CustomBottomSheet(
              builder: (_, controller) {
                return ListView.builder(
                  itemCount: state.length,
                  itemBuilder: (_, index) {
                    final country = state[index];
                    return ListTile(
                      onTap: () => widget.onChanged(country),
                      leading: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Image.asset(country.flag),
                          8.pw(),
                          Text(
                            '(+${country.phoneCode})',
                            style: CustomTextStyles.textMediumS.copyWith(
                              color: AppColors.gray.shade800,
                            ),
                          ),
                        ],
                      ),
                      title: Text(
                        country.countryName,
                        style: CustomTextStyles.textRegularS.copyWith(
                          color: AppColors.gray.shade900,
                        ),
                      ),
                    );
                  },
                  controller: controller,
                );
              },
            );
          },
        );
      },
    );
  }
}
