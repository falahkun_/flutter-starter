import 'package:auth/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:shared/shared.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginError) {
          AppLoading.hide();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(
                state.message.toMessage(context),
              ),
            ),
          );
        } else if (state is LoginSuccess) {
          AppLoading.hide();
          context.go(NamedRoutes.home);
        } else if (state is LoginLoading) {
          // do something
          AppLoading.show(context);
        }
      },
      child: const BaseAuthScreen(
        body: _Body(),
      ),
    );
  }
}

class _Body extends StatefulWidget {
  const _Body();

  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: ListView(
        children: [
          CustomTextFormField(
            controller: emailController,
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.email,
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
            ),
          ),
          CustomTextFormField(
            controller: passwordController,
            obscureText: true,
            decoration: CustomInputDecoration(
              backgroundColor: AppColors.gray.shade200.withOpacity(.5),
              labelText: context.l10n.password,
              hintText: '********',
              enabledBorderColor: Colors.transparent,
              focusedBorderColor: Colors.transparent,
              errorBorderColor: Colors.red,
            ),
            removePaddingBottom: true,
          ),
          const _ForgotPassword(),
          24.ph(),
          CustomPrimaryButton.label(
            context.l10n.login,
            onPressed: () {
              if (emailController.text.isEmpty) {
                return;
              }

              if (passwordController.text.isEmpty) {
                return;
              }

              final data = LoginRequestModel(
                email: emailController.text.trim(),
                password: passwordController.text.trim(),
              );
              context.read<LoginBloc>().add(
                    LoginWithEmailAndPassword(
                      requestData: data,
                    ),
                  );
            },
            width: double.maxFinite,
          ),
          32.ph(),
          const _RegisterButton(),
        ],
      ),
    );
  }
}

class _RegisterButton extends StatelessWidget {
  const _RegisterButton();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          context.l10n.doesntHaveAccount,
          style: CustomTextStyles.textRegularS.copyWith(
            color: AppColors.gray.shade900,
          ),
        ),
        TextButton(
          onPressed: () {
            context.push(NamedRoutes.register);
          },
          child: Text(
            context.l10n.register,
            style: CustomTextStyles.textMediumS.copyWith(
              color: AppColors.primary,
            ),
          ),
        ),
      ],
    );
  }
}

class _ForgotPassword extends StatelessWidget {
  const _ForgotPassword();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        TextButton(
          onPressed: () => context.push(NamedRoutes.forgotPassword),
          child: Text(
            context.l10n.forgotPassword,
            style: CustomTextStyles.textSemiboldS,
          ),
        ),
      ],
    );
  }
}

// class CustomTextFormField extends StatefulWidget {
//   const CustomTextFormField({
//     required this.controller,
//     super.key,
//   });

//   final TextEditingController controller;

//   @override
//   State<CustomTextFormField> createState() => _CustomTextFormFieldState();
// }

// class _CustomTextFormFieldState extends State<CustomTextFormField> {
//   final _emailController = TextEditingController();
//   final _passwordController = TextEditingController();

//   @override
//   Widget build(BuildContext context) {
//     final outlinedInputBorder = OutlineInputBorder(
//       borderRadius: BorderRadius.circular(16),
//       borderSide: BorderSide(
//         color: Colors.grey.shade700,
//       ),
//     );
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Text(
//           'Email',
//           style: Theme.of(context).textTheme.labelLarge,
//         ),
//         const SizedBox(
//           height: 4,
//         ),
//         TextFormField(
//           controller: _emailController,
//           decoration: InputDecoration(
//             fillColor: Colors.transparent,
//             enabledBorder: outlinedInputBorder,
//             focusedBorder: outlinedInputBorder,
//           ),
//         ),
//         const SizedBox(height: 16),
//         Text(
//           'Password',
//           style: Theme.of(context).textTheme.labelLarge,
//         ),
//         const SizedBox(
//           height: 4,
//         ),
//         TextFormField(
//           controller: _passwordController,
//           decoration: InputDecoration(
//             enabledBorder: outlinedInputBorder,
//             focusedBorder: outlinedInputBorder,
//             fillColor: Colors.transparent,
//             // enabledBorder: OutlinedBorder(),
//           ),
//         ),
//         const SizedBox(
//           height: 24,
//         ),
//         SizedBox(
//           width: double.maxFinite,
//           child: MaterialButton(
//             onPressed: () {
//               context.read<LoginBloc>().add(
//                     LoginWithEmailAndPassword(
//                       requestData: LoginRequestModel(
//                         email: _emailController.text,
//                         password: _passwordController.text.trim(),
//                       ),
//                     ),
//                   );
//             },
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(8),
//               side: BorderSide(
//                 color: Colors.grey.shade700,
//               ),
//             ),
//             height: 44,
//             color: Colors.lightBlue,
//             child: const Text('Sign In'),
//           ),
//         ),
//       ],
//     );
//   }
// }
