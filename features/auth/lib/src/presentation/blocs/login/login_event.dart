part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class LoginWithEmailAndPassword extends LoginEvent {
  const LoginWithEmailAndPassword({required this.requestData});

  final LoginRequestModel requestData;
}
