import 'package:auth/auth.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc(this.loginUseCase) : super(LoginInitial()) {
    on<LoginWithEmailAndPassword>(_loginWithEmailAndPassword);
  }

  Future<void> _loginWithEmailAndPassword(
    LoginWithEmailAndPassword event,
    Emitter<LoginState> emit,
  ) async {
    emit(LoginLoading());
    final result = await loginUseCase(event.requestData);
    result.fold(
      (failure) => emit(LoginError(message: failure.message)),
      (result) => emit(LoginSuccess()),
    );
  }

  final LoginUseCase loginUseCase;
}
