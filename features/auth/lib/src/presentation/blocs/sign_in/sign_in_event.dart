part of 'sign_in_bloc.dart';

abstract class SignInEvent extends Equatable {
  const SignInEvent();
}

class SignInWithEmailAndPassword extends SignInEvent {
  const SignInWithEmailAndPassword(this.email, this.password);

  final String email;
  final String password;

  @override
  List<Object?> get props => [email, password];
}
