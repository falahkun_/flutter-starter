import 'dart:async';
import 'dart:developer';

import 'package:auth/auth.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'sign_in_event.dart';

part 'sign_in_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  SignInBloc({required this.signInUseCase}) : super(SignInInitial()) {
    on<SignInWithEmailAndPassword>(_onSignInWithEmailAndPassword);
  }

  final SignInUseCase signInUseCase;

  Future<void> _onSignInWithEmailAndPassword(
    SignInWithEmailAndPassword event,
    Emitter<SignInState> emit,
  ) async {
    emit(SignInLoading());

    if (event.email.isEmpty || event.password.isEmpty) {
      emit(const SignInError('CREDENTIAL_INVALID'));
      return;
    }

    if (!isEmailValid(event.email.trim())) {
      emit(const SignInError('EMAIL_INVALID'));
      log(event.email);
      return;
    }

    final result = await signInUseCase(
      LoginRequestModel(email: event.email, password: event.password),
    );

    result.fold(
      (failure) => emit(SignInError(failure.message)),
      (result) => emit(SignInSuccess()),
    );
  }
}

bool isEmailValid(String email) {
  return RegExp(r'^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(email);
}
