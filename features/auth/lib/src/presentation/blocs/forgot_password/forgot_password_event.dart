part of 'forgot_password_bloc.dart';

abstract class ForgotPasswordEvent extends Equatable {
  const ForgotPasswordEvent();

  @override
  List<Object> get props => [];
}

class GetEmailVerificationEvent extends ForgotPasswordEvent {
  const GetEmailVerificationEvent({required this.requestData});

  final ForgotPasswordModel requestData;
}
