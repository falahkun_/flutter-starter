import 'package:auth/auth.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'forgot_password_event.dart';
part 'forgot_password_state.dart';

class ForgotPasswordBloc
    extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
  ForgotPasswordBloc(this.getForgotPasswordEmailUseCase)
      : super(ForgotPasswordInitial()) {
    on<GetEmailVerificationEvent>(_onGetEmailVerificationEvent);
  }

  Future<void> _onGetEmailVerificationEvent(
    GetEmailVerificationEvent event,
    Emitter<ForgotPasswordState> emit,
  ) async {
    final result = await getForgotPasswordEmailUseCase(event.requestData);
    result.fold(
      (failure) => emit(ForgotPasswordError(failure.message)),
      (result) => ForgotPasswordSuccess(),
    );
  }

  final GetForgotPasswordEmailUseCase getForgotPasswordEmailUseCase;
}
