part of 'register_bloc.dart';

class RegisterEvent extends Equatable {
  const RegisterEvent(this.requestData);

  final RegisterRequestModel requestData;

  @override
  List<Object> get props => [
        requestData,
      ];
}
