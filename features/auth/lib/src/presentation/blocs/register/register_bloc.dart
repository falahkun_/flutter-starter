import 'package:auth/auth.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc(this.registerUseCase) : super(RegisterInitial()) {
    on<RegisterEvent>(_onRegisterEvent);
  }

  Future<void> _onRegisterEvent(
    RegisterEvent event,
    Emitter<RegisterState> emit,
  ) async {
    emit(RegisterLoading());
    final result = await registerUseCase(event.requestData);
    result.fold(
      (failure) => emit(RegisterError(message: failure.message)),
      (result) => emit(RegisterSuccess()),
    );
  }

  final RegisterUseCase registerUseCase;
}
