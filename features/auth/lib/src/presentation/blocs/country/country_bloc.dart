import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared/shared.dart';

part 'country_event.dart';

class CountryBloc extends Bloc<CountryEvent, List<CountryCode>> {
  CountryBloc() : super(const <CountryCode>[]) {
    on<GetCountryEvent>(_onGetCountryEvent);
  }

  Future<void> _onGetCountryEvent(
    GetCountryEvent event,
    Emitter<List<CountryCode>> emit,
  ) async {
    emit(CountryCode.countries);
  }
}
