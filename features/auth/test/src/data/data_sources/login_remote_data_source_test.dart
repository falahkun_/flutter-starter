import 'dart:convert';

import 'package:auth/auth.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class DioMock extends Mock implements Dio {}

void main() {
  late Dio dio;
  late LoginRemoteDataSourceImpl loginRemoteDataSourceImpl;

  setUpAll(() {
    dio = DioMock();
    loginRemoteDataSourceImpl = LoginRemoteDataSourceImpl(dio);
  });

  const successResponse = '''
  {
  "status": true,
  "status_code": 200,
  "message": "sign in success",
  "total_data": 0,
  "data": {
    "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlIjoidXNlciJ9.p38cgBUACw973_1GKoC3X3PJjVZUh1LpMq6OwuprSMu5MhcOIHOuHy62TXZa0odAzX4cNna5oPfkzTbV0ugqDg"
  }
}
  ''';

  group('signIn test', () {
    test(
      'test sign if all credential valid and get reponse success',
      () async {
        const requestData = LoginRequestModel(
          email: 'user2@mailinator.com',
          password: '123456',
        );
        when(
          () => dio.post<Map<String, dynamic>>('v1/user/signin'),
        ).thenAnswer(
          (_) async => Response(
            requestOptions: RequestOptions(
              path: 'v1/user/signin',
              data: jsonDecode(successResponse),
            ),
          ),
        );
        final result = await loginRemoteDataSourceImpl.signIn(requestData);
        expect(result.status, true);
      },
    );
  });
}
